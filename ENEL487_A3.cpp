/**
		Name: Tessa Herzberger
		SID: 200342876
		
		Institution: University of Regina
		Class: ENEL 487
		Professor: Karim Naqvi

		Assignment: #3
		Due: 19 September 2017

*/

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include "dictionary.h"
#include <time.h>

using namespace std;

/**
	This is the main function. It asks for the user
	to enter a 0 if they want to quit or a 1 to generate a password.
	This process repeats until 0 is entered to exit the do-while loop and the program.
*/
int main()
{
	//Initialize the variables
	char user_input = 1;
	int rand_1 = 0;
	int rand_2 = 0;
	int length = 0;
	char password[25] = {'\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0',
							'\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'};

	//Seed the random number generator in order to ensure that the same numbers are not generated multiple times.
	srand(time(NULL));

	do
	{	
		//Prompt the user to enter a 0 or a 1.
		cerr << "Please enter 1 to generate a password." << endl;
		cerr << "Please enter 0 to quit." << endl;
		cin >> user_input;

		if (user_input == '0')
			break;
		
		//Determine if the user has entered a valid value.
		while (user_input != '0' && user_input != '1')
		{
			cerr << "You have entered an invalid value." << endl;

			//Prompt the user to enter a 0 or a 1.
			cerr << "Please enter 1 to generate a password." << endl;
			cerr << "Please enter 0 to quit." << endl;
			cin >> user_input;
		}

		for (int i = 0; i < 5; i++)
		{
			//Re-initialize length to 0.
			length = 0;

			//Generate the two random numbers
			rand_1 = rand();
			rand_2 = rand();

			//Make sure rand_1 is positive
			if (rand_1 < 0)
				rand_1 = rand_1 * -1;

			//Make sure rand_2 is positive
			if (rand_2 < 0)
				rand_2 = rand_2 * -1;

			//Concatinate the two random numbers together.
			while (rand_2 > 10)
			{
				rand_1 *= 10;
				rand_1 += (rand_2 % 10);
				rand_2 /= 10;
			}

			//Make sure rand_1 is positive
			if (rand_1 < 0)
				rand_1 = rand_1 * -1;

			//Ensure the random number is within the length of the password_dictionary array
			rand_1 %= 642705;

			if (rand_1 >= 642695)
				rand_1 -= 20;

			//Search for the start of the next word
			while(password_dictionary[rand_1] != '\n')
				rand_1++;

			//Make sure rand_1 is positive
			if (rand_1 < 0)
				rand_1 = rand_1 * -1;

			//Copy the word from password_dictionary into the password array
			while(password_dictionary[rand_1+1] != '\n')
			{
				password[length] = password_dictionary[rand_1+1];
				length++;
				rand_1++;
			}

			//Print the password
			for (int i = 0; i < length-1; i++)
				cerr << password[i];

			cerr << " ";
		} 
		cerr << endl;
	}while (user_input != '0');

	return 0;	
}