input = open("preprocessed.txt", 'r')
output = open("dictionary.txt", 'w')
length = 0;

for line in input:
                
    for character in line:
        
        length = length + 1

        if (character == "\n"):
            output.write("'/n'")
            output.write("\n")
            output.write(",")
                        
        else:
            output.write("'")
            output.write(character)
            output.write("'")
            output.write(",")

output.write(str("LENGTH: "))
output.write(str(length))

input.close()
output.close()
